//
//  ProductModel.swift
//  Raybaby
//
//  Created by logeshB on 08/05/19.
//  Copyright © 2019 com.raybay. All rights reserved.
//

import Foundation

class ProdModel {
    var products = [Product]()
    static let shared = ProdModel()
}

class CartModel {
    var cartProducts = [Product]()
    static let shared = CartModel()
}


