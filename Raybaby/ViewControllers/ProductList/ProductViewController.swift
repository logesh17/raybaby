//
//  ProductViewController.swift
//  Raybaby
//
//  Created by logeshB on 08/05/19.
//  Copyright © 2019 com.raybay. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {
    
    @IBOutlet weak var productTableView: UITableView!
    
    let barButton = BarButtonBage()
    
    var products = [Product]()
    
    let nc = NotificationCenter.default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureTableView()
        configureData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateBadgecount()
        productTableView.reloadData()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    func configureTableView(){
        productTableView.estimatedRowHeight = 100.0
        productTableView.rowHeight = UITableView.automaticDimension
        productTableView.tableFooterView = UIView()
    }
    
    //Loads data from json to shared product instance
    func configureData(){
        ProdModel.shared.products = loadJson(filename: "product") ?? [Product]()
        nc.addObserver(self, selector: #selector(cartAlert), name: Notification.Name("cartAlert"), object: nil)
    }
    
    @objc func cartAlert(){
        show1ButtonAlert("Alert", message: "Item added to cart")
    }
    
    @objc func presentCart() {
        if CartModel.shared.cartProducts.count <= 0 {
            show1ButtonAlert("Alert", message: "Please add items to cart,your cart is empty")
        }else{
            presentCartVC()
        }
    }
    
    
    @IBAction func addToCartTapped(_ sender: UIButton) {
        if ProdModel.shared.products[sender.tag].isAddedtoCart == false {
            ProdModel.shared.products[sender.tag].isAddedtoCart = true
            CartModel.shared.cartProducts.append(ProdModel.shared.products[sender.tag])
            updateBadgecount()
            nc.post(name: Notification.Name("cartAlert"), object: nil)
        }else {
            removeItemfromCart(tag: sender.tag)
        }
        self.productTableView.reloadData()
    }
    
    @IBAction func displayActionSheet(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "Sort By", preferredStyle: .actionSheet)
        
        //Sorting high to low
        let sortLowtoHigh = UIAlertAction(title: "Price: Low to High", style: .default) { (action) in
            self.sort(using: "lowtohigh")
        }
        sortLowtoHigh.setValue(UIColor(rgb: 0x212121), forKey: "titleTextColor")
        
        //Sorting low to high
        let sortHightoLow = UIAlertAction(title: "Price: High to Low", style: .default) { (action) in
            self.sort(using: "hightolow")
        }
        sortHightoLow.setValue(UIColor(rgb: 0x212121), forKey: "titleTextColor")

        //Sorting using name
        let sortProdName = UIAlertAction(title: "Name", style: .default) { (action) in
           self.sort(using: "name")
        }
        sortProdName.setValue(UIColor(rgb: 0x212121), forKey: "titleTextColor")
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        cancelAction.setValue(UIColor(rgb: 0x212121), forKey: "titleTextColor")

        optionMenu.addAction(sortLowtoHigh)
        optionMenu.addAction(sortHightoLow)
        optionMenu.addAction(sortProdName)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func sort(using : String){
        let products = ProdModel.shared.products.sorted(by: { (var1, var2) -> Bool in
            switch using {
            case "lowtohigh":
                return var1.price < var2.price
            case "hightolow":
                return var1.price > var2.price
            default:
                return var1.productName < var2.productName
            }
        })
        ProdModel.shared.products = products
        self.productTableView.reloadData()
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        CartModel.shared.cartProducts.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    
    // Helper Functions
    func presentCartVC(){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc : CartViewController = storyboard.instantiateViewController(withIdentifier: "cartVC") as! CartViewController
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.navigationBar.barTintColor = UIColor(rgb: 0x212121)
        navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController.navigationBar.barStyle = .black
        self.present(navigationController, animated: true, completion: nil)
    }
    
    //Updates badge count in barButton
    func updateBadgecount() {
        let itemsCount = CartModel.shared.cartProducts.count
        setBarbtnWithBadge(badgeValue: itemsCount)
        barButton.badgeLabel.isHidden = (itemsCount > 0) ? false : true
    }
    
    func setBarbtnWithBadge(badgeValue : Int) {
        barButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
        barButton.setImage(UIImage(named: "cartImage")?.withRenderingMode(.alwaysTemplate), for: .normal)
        barButton.tintColor = UIColor.white
        barButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 15)
        barButton.badge = String(badgeValue)
        barButton.addTarget(self, action: #selector(presentCart), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButton)
    }
    
    //Deselecting item from cart
    func removeItemfromCart(tag : Int){
        ProdModel.shared.products[tag].isAddedtoCart = false
        let prodToRemove = ProdModel.shared.products[tag].productId
        var idx = 0
        for i in CartModel.shared.cartProducts{
            if i.productId == prodToRemove {
                CartModel.shared.cartProducts.remove(at:idx)
                updateBadgecount()
                break
            } else {
                idx += 1
            }
        }
        
    }
}

extension ProductViewController : UITableViewDataSource,UITableViewDelegate{
    //Tableview helper methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ProdModel.shared.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ProductTableViewCell else {
            return UITableViewCell()
        }
        
        cell.prodName.text = ProdModel.shared.products[indexPath.row].productName
        cell.prodPrice.text = "$\(String(ProdModel.shared.products[indexPath.row].price))"
        cell.addtoCartBtn.tag = indexPath.row
        if ProdModel.shared.products[indexPath.row].isAddedtoCart == true {
            cell.addtoCartBtn.setImage(UIImage(named: "cartFilled"), for: .normal)
        }else{
            cell.addtoCartBtn.setImage(UIImage(named: "cartLine"), for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toProdDetail", sender: nil)
    }
}
