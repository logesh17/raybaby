//
//  ProductTableViewCell.swift
//  Raybaby
//
//  Created by logeshB on 08/05/19.
//  Copyright © 2019 com.raybay. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var prodName: UILabel!
    
    @IBOutlet weak var prodPrice: UILabel!
    
    @IBOutlet weak var addtoCartBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
