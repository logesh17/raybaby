//
//  CartViewController.swift
//  Raybaby
//
//  Created by logeshB on 09/05/19.
//  Copyright © 2019 com.raybay. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {

    @IBOutlet weak var cartTableView: UITableView!
    
    @IBOutlet weak var cartTotal: UILabel!
    
    var priceArray = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        cartTableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateTotal()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }

    @IBAction func checkoutAction(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Item Purchased", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            for i in 0..<ProdModel.shared.products.count{
                ProdModel.shared.products[i].isAddedtoCart = false
            }
            CartModel.shared.cartProducts.removeAll()
            self.dismiss(animated: true, completion: nil)
            print("Ok")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteBtnAction(_ sender: UIButton) {
        let prodToRemove = CartModel.shared.cartProducts[sender.tag].productId
        CartModel.shared.cartProducts.remove(at: sender.tag)
        cartTableView.reloadData()
        for i in 0..<ProdModel.shared.products.count{
            if ProdModel.shared.products[i].productId == prodToRemove {
                ProdModel.shared.products[i].isAddedtoCart = false
                break
            }
        }
    }
    
    func updateTotal() {
        priceArray.removeAll()
        for i in 0..<CartModel.shared.cartProducts.count {
            priceArray.append(Int(CartModel.shared.cartProducts[i].price))
        }
        let totalCartValue = priceArray.reduce(0, +)
        cartTotal.text = "$" + String(totalCartValue)
    }
    
}

extension CartViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CartModel.shared.cartProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell", for: indexPath) as? CartTableViewCell else {
            return UITableViewCell()
        }
        cell.itemName.text = CartModel.shared.cartProducts[indexPath.row].productName
        cell.itemPrice.text = "$\(String(CartModel.shared.cartProducts[indexPath.row].price))"
        cell.deleteBtn.tag = indexPath.row
        return cell
    }
    
    
}
