//
//  LoginViewController.swift
//  Raybaby
//
//  Created by logeshB on 09/05/19.
//  Copyright © 2019 com.raybay. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usrNameTxtField: UITextField!
    
    @IBOutlet weak var passwrdTxtField: UITextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if usrNameTxtField.text!.isEmpty || passwrdTxtField.text!.isEmpty {
            show1ButtonAlert("Alert", message: "Username/Password cannot be empty")
        }else{
            self.performSegue(withIdentifier: "toLogin", sender: nil)
        }
    }
}


