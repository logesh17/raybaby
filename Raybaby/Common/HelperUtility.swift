//
//  HelperUtility.swift
//  Raybaby
//
//  Created by logeshB on 09/05/19.
//  Copyright © 2019 com.raybay. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{
    func show1ButtonAlert(_ title: String?, message: String?, buttonTitle: String = NSLocalizedString("OK", comment: "OK"), handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: buttonTitle, style: .cancel, handler: handler)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
}


