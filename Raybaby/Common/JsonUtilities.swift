//
//  JsonUtilities.swift
//  Raybaby
//
//  Created by logeshB on 08/05/19.
//  Copyright © 2019 com.raybay. All rights reserved.
//

import Foundation

struct ResponseData: Codable {
    let product: [Product]
}

struct Product: Codable {
    let productName: String
    let price: Int
    var isAddedtoCart : Bool
    let productId : Int
}

func loadJson(filename fileName: String) -> [Product]? {
    if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let jsonData = try decoder.decode(ResponseData.self, from: data)
            return jsonData.product
        } catch {
            print("error:\(error)")
        }
    }
    return nil
}

